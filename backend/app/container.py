from app.database import db
from app.dao.genre import GenreDAO
from app.dao.director import DirectorDAO
from app.dao.movie import MovieDAO
from app.dao.user import UserDAO
from app.service.genre import GenreService
from app.service.director import DirectorService
from app.service.movie import MovieService
from app.service.user import UserService


genre_dao = GenreDAO(db.session)
director_dao = DirectorDAO(db.session)
movie_dao = MovieDAO(db.session)
user_dao = UserDAO(db.session)

genre_service = GenreService(genre_dao)
director_service = DirectorService(director_dao)
movie_service = MovieService(movie_dao)
user_service = UserService(user_dao)



