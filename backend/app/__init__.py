from flask import Flask 
from flask_restx import Api
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
from flask_cors import CORS

from app.database import db
from app.config import DevelopmentConfig

from app.view.director import director_ns
from app.view.genre import genre_ns
from app.view.movie import movie_ns
from app.view.user import user_ns
from app.view.auth import auth_ns



def create_app():
    app = Flask(__name__)
    app.config.from_object(DevelopmentConfig)
    register_extension(app=app)

    return app


def register_extension(app):
    db.init_app(app=app)
    migrate = Migrate(app, db)
    jwt = JWTManager(app=app)
    cors = CORS(app=app)
    api = Api(app, doc='/docs')
    api.add_namespace(director_ns)
    api.add_namespace(genre_ns)
    api.add_namespace(movie_ns)
    api.add_namespace(auth_ns)
    api.add_namespace(user_ns)

