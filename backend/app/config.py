from pathlib import Path
from datetime import timedelta

BASE_DIR = Path(__file__).parents[1]


class Config:
    SECRET_KEY = 'aefded92a0ce95719df5c1904c144669a9e1eb2488640afcee172a618ba678a0'
    JWT_SECRET_KEY = 'd75b4fa29a3ac973abb1ced04cec9a7b9ff035b04285ade6475e043250613da4'
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=30)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=90)
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///./dbase.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEBUG = True
    DEVELOPMENT = True


class TestingConf(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True




class ProdactionConfig(Config):
    ...
