from app.dao.model.user import User


class UserDAO:

    def __init__(self, session):
        self.session = session

    def get_all(self):
        return self.session.query(User).all()

    def get_one(self, pk):
        return self.session.query(User).get_or_404(pk)

    def get_username(self, username):
        return self.session.query(User).filter(User.name==username).first()

    def get_email(self, email):
        return self.session.query(User).filter(User.email==email).first()


    def create(self, data):
        user = User(**data)
        
        self.session.add(user)
        self.session.commit()

    def update(self, data):
        self.session.add(data)
        self.session.commit()

    def delete(self, pk):
        self.session.delete(self.get_one(pk))
        self.session.commit()


