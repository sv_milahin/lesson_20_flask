from app.dao.model.director import Director


class DirectorDAO:

    def __init__(self, session):
        self.session = session

    def get_all(self):
        return self.session.query(Director).all()

    def get_one(self, pk):
        return self.session.query(Director).get_or_404(pk)

    def create(self, data):
        director = Director(**data)
        
        self.session.add(director)
        self.session.commit()

    def update(self, data):
        self.session.add(data)
        self.session.commit()

    def delete(self, pk):
        self.session.delete(self.get_one(pk))
        self.session.commit()


