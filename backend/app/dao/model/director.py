from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

from app.database import db


class Director(db.Model):
    __tablename__ = 'directors'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), nullable=False)

    def __repr__(self):
        return f'<Director({self.name=})>'


class DirectorSchema(SQLAlchemySchema):
    class Meta:
        model = Director
        load_instance = True
    
    id = auto_field()
    name = auto_field()

director_schema = DirectorSchema()
directors_schema = DirectorSchema(many=True)
