from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from werkzeug.security import generate_password_hash

from app.database import db


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), nullable=False)
    email = db.Column(db.String(128), nullable=False, unique=True)
    password = db.Column(db.String(512), nullable=False)
    role = db.Column(db.String(16), nullable=False)

    def __init__(self, name, email, password, role):
        self.name = name
        self.email = email
        self.password = generate_password_hash(password=password)
        self.role = role
    
    def __repr__(self):
        return f'<User({self.name=})>'


class UserSchema(SQLAlchemySchema):
    class Meta:
        model = User
        load_instance = True
    
    id = auto_field()
    name = auto_field()
    email = auto_field()
    password = auto_field()
    role = auto_field()

user_schema = UserSchema()
users_schema = UserSchema(many=True)

