from app.dao.model.genre import Genre


class GenreDAO:

    def __init__(self, session):
        self.session = session

    def get_all(self):
        return self.session.query(Genre).all()

    def get_one(self, pk):
        return self.session.query(Genre).get_or_404(pk)

    def create(self, data):
        genre = Genre(**data)
        
        self.session.add(genre)
        self.session.commit()

    def update(self, data):
        self.session.add(data)
        self.session.commit()

    def delete(self, pk):
        self.session.delete(self.get_one(pk))
        self.session.commit()


