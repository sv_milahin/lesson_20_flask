from app.dao.user import UserDAO


class UserService:
    def __init__(self, dao):
        self.dao = dao

    def get_all(self):
        return self.dao.get_all()

    def get_one(self, pk):
        return self.dao.get_one(pk)

    def get_username(self, username):
        return self.dao.get_username(username)

    def create(self, data):
        return self.dao.create(data)

    def get_email(self, email):
        return self.dao.get_email(email)


    def update(self, data):
        pk = data.get('id') 
        user = self.get_one(pk)
        user.name = data.get('name')
        user.email = data.get('email')
        user.role = data.get('role')
        user.password = data.get('password')

        self.dao.update(user)

    def update_partial(self, data):
        pk = data.get('id')
        user = self.get_one(pk)

        if 'name' in data:
            user.name = data.get('name')
        if 'email' in data:
            user.email = data.get('email')
        if 'password' in data:
            user.password = data.get('password')
      
        self.dao.update(user)


    def delete(self, pk):
        self.dao.delete(pk)
