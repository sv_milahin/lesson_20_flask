from app.dao.director import DirectorDAO


class DirectorService:
    def __init__(self, dao):
        self.dao = dao

    def get_all(self):
        return self.dao.get_all()

    def get_one(self, pk):
        return self.dao.get_one(pk)

    def create(self, data):
        return self.dao.create(data)

    def update(self, data):
        pk = data.get('id') 
        director = self.get_one(pk)
        director.name = data.get('name')
      
        self.dao.update(director)

    def delete(self, pk):
        self.dao.delete(pk)
