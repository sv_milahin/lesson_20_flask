from flask import abort, app, request
from flask_restx import Resource, Namespace
from flask_jwt_extended import jwt_required, get_jwt

from app.dao.model.director import director_schema, directors_schema
from app.container import director_service


director_ns = Namespace('directors')


@director_ns.route('/')
class DirectorsView(Resource):
    @jwt_required()
    def get(self):
        director = director_service.get_all()
        return directors_schema.dump(director), 200
    @jwt_required()
    def post(self):
        req = request.get_json()
        role_admin = get_jwt()
        if role_admin['role'] == 'admin': 
            director = director_service.create(req)
            return '', 201, {'location': f'/directors/{director}'}
        abort(401)

@director_ns.route('/<int:pk>')
class DirectorView(Resource):
    @jwt_required()
    def get(self, pk):
        director = director_service.get_one(pk)
        return director_schema.dump(director), 200
    @jwt_required()
    def put(self, pk):
        req = request.get_json()
        req['id'] = pk
        role_admin = get_jwt()
        if role_admin['role'] == 'admin':
            director_service.update(req)
            return '', 200
        abort(401)

    @jwt_required()
    def delete(self, pk):
        role_admin = get_jwt()
        if role_admin['role'] == 'admin':
            director_service.delete(pk)
            return '', 204 
        abort(401)
