from flask import abort, request
from flask_restx import Resource, Namespace
from flask_jwt_extended import jwt_required, get_jwt

from app.dao.model.user import user_schema, users_schema
from app.container import user_service


user_ns = Namespace('users')


@user_ns.route('/')
class UsersView(Resource):
    @jwt_required()
    def get(self):
        user = user_service.get_all()

        return users_schema.dump(user), 200


@user_ns.route('/<int:pk>')
class UserView(Resource):
    @jwt_required()
    def get(self, pk):
        user = user_service.get_one(pk)
        return user_schema.dump(user), 200
    @jwt_required()
    def put(self, pk):
        req = request.get_json()
        req['id'] = pk

        user_service.update(req)
        return '', 200
    @jwt_required()
    def delete(self, pk):
        role_admin = get_jwt()
        if role_admin['role'] == 'admin':
            user_service.delete(pk)
            return '', 204
        abort(401) 


@user_ns.route('/<string:username>')
class UserNameView(Resource):
    @jwt_required()
    def get(self, username):
        user = user_service.get_username(username)
        if not user:
            abort(404)
        return user_schema.dump(user), 201
