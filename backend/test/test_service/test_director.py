import pytest
from unittest.mock import MagicMock

from app.database import db
from app.dao.model.director import Director
from app.dao.director import DirectorDAO
from app.service.director import DirectorService

    
@pytest.fixture()
def director_dao():
    director_dao = DirectorDAO(db.session)

    jonh = Director(name='jonh')
    kate = Director(name='kate')
    max = Director(name='max')

    director_dao.get_one = MagicMock(return_value=jonh)
    director_dao.get_all = MagicMock(return_value=[jonh, kate, max])
    director_dao.create = MagicMock()
    director_dao.delete = MagicMock()
    director_dao.update = MagicMock()
    return director_dao


class TestDirectorService:
    @pytest.fixture(autouse=True)
    def director_service(self, director_dao):
        self.director_service = DirectorService(dao=director_dao)

    def test_get_one(self):
        director = self.director_service.get_one(1)
        assert director != None
       # assert director.id == 1
    def test_get_all(self):
        directors = self.director_service.get_all()
        assert len(directors) > 0

    def test_create(self):
        director_d = {
            "id": 20,
            "name": "Ivan",
    
        }
        director = self.director_service.create(director_d)
        assert director.id != None

    def test_delete(self):
        self.director_service.delete(1)

    def test_update(self):
        director_d = {
            "id": 4,
            "name": "Petr",

        }
        self.director_service.update(director_d)


