import pytest
from unittest.mock import MagicMock

from app.database import db
from app.dao.model.user import User
from app.dao.user import UserDAO
from app.service.user import UserService

    
@pytest.fixture()
def user_dao():
    user_dao = UserDAO(db.session)

    jonh = User(name='jonh', email='test1@mail.ru', password='123', role='admin')
    kate = User(name='kate', email='test2@mail.ru', password='123', role='user')
    max = User(name='max', email='test3@mail.ru', password='123', role='user')

    user_dao.get_one = MagicMock(return_value=jonh)
    user_dao.get_all = MagicMock(return_value=[jonh, kate, max])
    user_dao.create = MagicMock()
    user_dao.delete = MagicMock()
    user_dao.update = MagicMock()
    return user_dao


class TestUserService:
    @pytest.fixture(autouse=True)
    def user_service(self, user_dao):
        self.user_service = UserService(dao=user_dao)

    def test_get_one(self):
        user = self.user_service.get_one(1)
        assert user != None
       # assert user.id == 1
    def test_get_all(self):
        users = self.user_service.get_all()
        assert len(users) > 0

    def test_create(self):
        user_d = {
            "id": 20,
            "name": "Ivan",
            "email": "test4@mail.ru",
            "password": '123',
            "role": 'user'
        }
        user = self.user_service.create(user_d)
        assert user.id != None

    def test_delete(self):
        self.user_service.delete(1)

    def test_update(self):
        user_d = {
            "id": 4,
            "name": "Petr",
            "email": "test4@mail.ru",
            "password": '123',
            "role": 'user'
        }
        self.user_service.update(user_d)


