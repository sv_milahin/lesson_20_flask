import pytest
from unittest.mock import MagicMock

from app.database import db
from app.dao.model.movie import Movie
from app.dao.movie import MovieDAO
from app.service.movie import MovieService

    
@pytest.fixture()
def movie_dao():
    movie_dao = MovieDAO(db.session)

    test_movie1 = Movie(
            title = "Test title 1",
            description = "Test description 1",
            trailer = "Test trailer 1",
            year = "2022",
            rating = "8.9",
            genre_id = "2",
            director_id = "4"
            )
    test_movie2 = Movie(
            title = "Test title 2",
            description = "Test description 2",
            trailer = "Test trailer 2",
            year = "2022",
            rating = "8.9",
            genre_id = 2,
            director_id = 4
            )
    test_movie3 = Movie(
            title = "Test title 3",
            description = "Test description 3",
            trailer = "Test trailer 3",
            year = "2022",
            rating = "8.9",
            genre_id = 2,
            director_id = 4
            )

    movie_dao.get_one = MagicMock(return_value=test_movie1)
    movie_dao.get_all = MagicMock(return_value=[test_movie1, test_movie2, test_movie3])
    movie_dao.create = MagicMock()
    movie_dao.delete = MagicMock()
    movie_dao.update = MagicMock()
    return movie_dao


class TestMovieService:
    @pytest.fixture(autouse=True)
    def movie_service(self, movie_dao):
        self.movie_service = MovieService(dao=movie_dao)

    def test_get_one(self):
        movie = self.movie_service.get_one(1)
        assert movie != None
       # assert movie.id == 1
    def test_get_all(self):
        movies = self.movie_service.get_all()
        assert len(movies) > 0

    def test_create(self):
        movie_d = {
            'title': "Test title 4",
            'description': "Test description 4",
            'trailer': "Test trailer 4",
            'year': "2022",
            'rating': "8.9",
            'genre_id': 2,
            'director_id': 4
        }
        movie = self.movie_service.create(movie_d)
        assert movie.id != None

    def test_delete(self):
        self.movie_service.delete(1)

    def test_update(self):
        movie_d = {
            'id':20,
            'title': "Test title 5",
            'description': "Test description 5",
            'trailer': "Test trailer 5",
            'year': "2024",
            'rating': "8.9",
            'genre_id': 2,
            'director_id': 4
        }
        self.movie_service.update(movie_d)


